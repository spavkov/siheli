/*
  Copyright (c) 2024 Spencer Pavkovic

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef CHI_H_
#define CHI_H_

#define CHI_BUF_SIZE 512


// Usage: printf(Chi_fmt"\n", Chi_arg(x));
#define Chi_fmt    "%.*s"
#define Chi_arg(X) (int)(X).l, (X).s

#define chi_free(CHI) \
  do {                \
    free((CHI).s);    \
    (CHI).l = 0;      \
  } while (0)

typedef struct {
    size_t l; // Length
    char *s;  // String
} Chi;

// Chops a string up to the next delim character, out can be NULL to discard
void chi_chop_to_delim(Chi *in, char delim, Chi *out);

// Chops a string up to the next space character, out can be NULL to discard
void chi_chop_to_space(Chi *in, Chi *out);

// Chops a string up to the next non-alphanumeric, out can be NULL to discard
void chi_chop_word(Chi *in, Chi *out);

// Returns 1 if a and b are equal, 0 otherwise
int chi_cmp(Chi *a, Chi *b);

// Modify a chi string to point to a new cstr
void chi_from_cstr(Chi *x, char *cstr);

// Create a new chi string on the stack from a cstr
inline Chi chi_new(char *cstr);

// Returns 1 if chi string is an ASCII letter/number/space character
inline int chi_is_letter(char c);
inline int chi_is_number(char c);
inline int chi_is_space(char c);

// Reads an entire file into a chi string
int chi_read_file(Chi *x, char *filepath);

// Convert ASCII to lowercase or uppercase respectively
void chi_to_lower(Chi *x);
void chi_to_upper(Chi *x);

// returns the length of a cstring
size_t chi_cstrlen(char *cstr);

#endif // CHI_H_

#ifdef CHI_IMPLEMENTATION

#include <stdio.h>  // For file I/O
#include <stdlib.h> // For realloc

void chi_chop_to_delim(Chi *in, char delim, Chi *out)
{
    char *begin = in->s;
    size_t len  = 0;

    while (*in->s != delim) {
        --in->l;
        ++in->s;
        ++len;
        if (in->l == 0)
            goto defer;
    }

    ++in->s;
    --in->l;

defer:
    if (out != NULL) {
        out->l = len;
        out->s = begin;
    }
}

void chi_chop_to_space(Chi *in, Chi *out)
{
    // Skip to the start of the next word
    while (chi_is_space(*in->s)) {
        --in->l;
        ++in->s;
        if (in->l == 0)
            return;
    }

    char *begin = in->s;
    size_t len  = 0;
    while (!chi_is_space(*in->s)) {
        --in->l;
        ++in->s;
        ++len;
        if (in->l == 0)
            goto defer;
    }

    --in->l;
    ++in->s;

defer:
    if (out != NULL) {
        out->l = len;
        out->s = begin;
    }
}

void chi_chop_word(Chi *in, Chi *out)
{
    // Skip to the start of the next word
    while (!(chi_is_letter(*in->s) || chi_is_number(*in->s))) {
        --in->l;
        ++in->s;
        if (in->l == 0)
            return;
    }

    char *begin = in->s;
    size_t len  = 0;
    while ((chi_is_letter(*in->s) || chi_is_number(*in->s))) {
        --in->l;
        ++in->s;
        ++len;
        if (in->l == 0)
            goto defer;
    }

    --in->l;
    ++in->s;

defer:
    if (out != NULL) {
        out->l = len;
        out->s = begin;
    }
}

int chi_cmp(Chi *a, Chi *b)
{
    if (a->l != b->l)
        return 0;

    for (size_t i = 0; i < a->l; ++i) {
        if (a->s[i] != b->s[i])
            return 0;
    }

    return 1;
}

void chi_from_cstr(Chi *x, char *cstr)
{
    x->s = cstr;
    x->l = 0;
    while (cstr[x->l])
        ++x->l;
}

inline Chi chi_new(char *cstr)
{
    return (Chi) {
        .l = chi_cstrlen(cstr),
        .s = cstr,
    };
}

inline int chi_is_letter(char c)
{
    return (('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z'));
}

inline int chi_is_number(char c)
{
    return ('0' <= c && c <= '9');
}

inline int chi_is_space(char c)
{
    return (c == ' ' || c == '\t' || c == '\n' || c == '\0');
}

int chi_read_file(Chi *x, char *filepath)
{
    FILE *file = fopen(filepath, "r");
    size_t chars_read = 0;
    char buf[CHI_BUF_SIZE];

    if (file == NULL) {
        fprintf(stderr, "chi_read_file: Could not read file %s\n", filepath);
        return 1;
    }

    x->l = 0;

    do {
        chars_read = fread(buf, sizeof(char), sizeof(buf), file);
        x->l += chars_read;

        x->s = realloc(x->s, x->l);
        if (x->s == NULL) {
            fprintf(stderr, "chi_read_file: Could not allocate space for chi string\n");
            fclose(file);
            return 2;
        } else {
            for (size_t i = 0; i <= chars_read; ++i)
                x->s[x->l - i] = buf[chars_read - i];
        }
    } while (chars_read != 0);

    fclose(file);
    return 0;
}

void chi_to_lower(Chi *x)
{
    for (size_t i = 0; i < x->l; ++i) {
        if ('A' <= x->s[i] && x->s[i] <= 'Z') {
            x->s[i] += 32;
        }
    }
}

void chi_to_upper(Chi *x)
{
    for (size_t i = 0; i < x->l; ++i) {
        if ('a' <= x->s[i] && x->s[i] <= 'z') {
            x->s[i] -= 32;
        }
    }
}

size_t chi_cstrlen(char *cstr)
{
    size_t len = 0;
    while (cstr[len])
        ++len;
    return len;
}

#endif // CHI_IMPLEMENTATION
