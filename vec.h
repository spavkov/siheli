/*
 * Copyright (c) 2024 Spencer Pavkovic
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef VEC_H_
#define VEC_H_

#ifndef VEC_ASSERT
#  include <assert.h>
#  define VEC_ASSERT assert
#endif

#ifndef VEC_REALLOC
#  include <stdlib.h>
#  define VEC_REALLOC realloc
#endif

/*
 * Provided vectors:
 *    Vec_char   = char
 *    Vec_uChar  = unsigned char
 *    Vec_short  = short
 *    Vec_uShort = unsigned short
 *    Vec_int    = int
 *    Vec_uInt   = unsigned int
 *    Vec_long   = long
 *    Vec_uLong  = unsigned long
 *    Vec_float  = float
 *    Vec_double = double
 */

#define VEC_START_S 128 /** Initial size of the vector when starting from 0. */
#define VEC_BL { .data = NULL, .c = 0, .s = 0 } /** Designated initializer to zero everything. */

/**
 * Push a value VAL to a vector ponter V.
 *
 * First, check to make sure that V has enough space for VAL. If it doesn't, check if the array size
 * is zero. If it is zero, expand to VEC_START_S, otherwise double in space. If the array was
 * expanded, assert to make sure it was correctly sized. Finally, append VAL to the array.
 *
 * @param V   Pointer to a vector
 * @param VAL Value to push to V
 */
#define vec_push(V, VAL)                                                 \
  do {                                                                   \
    if ((V)->c >= (V)->s) {                                              \
        size_t _O = (V)->s;                                              \
        (V)->s = (V)->s == 0 ? VEC_START_S : (V)->s * 2;                 \
        VEC_ASSERT((V)->s > _O && "Array size overflow");                \
        (V)->data = VEC_REALLOC((V)->data, (V)->s * sizeof(*(V)->data)); \
        VEC_ASSERT((V)->data != NULL && "Failed to realloc memory.");    \
    }                                                                    \
    (V)->data[(V)->c++] = (VAL);                                         \
  } while (0)

/**
 * Push ARR of size SZ to a vector pointer V.
 *
 * First, check to make sure that V has enough space to add SZ elements.
 * If it doesn't, check if the array size is zero. If it is zero, expand to VEC_START_S, otherwise
 * double in space. If the array was expanded, asert to make sure it was correctly sized.
 * Now go back, and check that V has enough space to add SZ elements, otherwise repeat.
 *
 * Once the vector is properly sized, up the count by SZ elements and read every element of ARR into
 * vector V.
 *
 * @param V   Pointer to a vector
 * @param ARR An array
 * @param SZ  The length of ARR
 */
#define vec_push_arr(V, ARR, SZ)                                           \
  do {                                                                     \
      while ((V)->s - (V)->c < SZ) {                                       \
          size_t _O = (V)->s;                                              \
          (V)->s = (V)->s == 0 ? VEC_START_S : (V)->s * 2;                 \
          VEC_ASSERT((V)->s > _O && "Array size overflow");                \
          (V)->data = VEC_REALLOC((V)->data, (V)->s * sizeof(*(V)->data)); \
          VEC_ASSERT((V)->data != NULL && "Failed to realloc memory.");    \
      }                                                                    \
      size_t _IC = (V)->c;                                                 \
      (V)->c += SZ;                                                        \
      for (size_t _Z = 0; _Z < SZ; ++_Z) {                                 \
          (V)->data[_IC + _Z] = ARR[_Z];                                   \
      }                                                                    \
  } while (0)

/**
 * Pop the last element off the end of the vector.
 *
 * Assert the array is not of count zero, decriment the count, and read off the last element.
 *
 * @param V Pointer to a vector
 */
#define vec_pop(V) (VEC_ASSERT((V)->c > 0), --(V)->c, (V)->data[(V)->c])

/**
 * Free a vector and set it's count and size to zero.
 *
 * @param V Vector
 */
#define vec_free(V)      \
  do {                   \
    if ((V).data) {      \
        free((V).data);  \
        (V).data = NULL; \
    }                    \
    (V).c = 0;           \
    (V).s = 0;           \
  } while (0)

/**
 * Define a new vector with name Vec_NAME and type TYPE.
 *
 * @param NAME The name of the new vector
 * @param TYPE The type of the new vector
 */
#define VEC_NEW(NAME, TYPE) \
  typedef struct {          \
      TYPE *data;           \
      size_t c;             \
      size_t s;             \
  } Vec_##NAME


VEC_NEW(uChar, unsigned char);
VEC_NEW(char, char);
VEC_NEW(uShort, unsigned short);
VEC_NEW(short, short);
VEC_NEW(uInt, unsigned int);
VEC_NEW(int, int);
VEC_NEW(uLong, unsigned long);
VEC_NEW(long, long);
VEC_NEW(float, float);
VEC_NEW(double, double);

#ifdef VEC_IMPLEMENTATION
#  include <stdio.h>
#endif

#ifndef _STDIO_H
typedef void FILE;
#endif

/**
 * Read the entire file f into vector v.
 *
 * @param v Pointer to Vec_char
 * @param f Pointer to FILE
 * @return  0 on success, -1 on failure
 */
int vec_read_entire_file(Vec_char *v, FILE *f);

/**
 * Read the file f into vector v up to size max.
 *
 * @param v   Pointer to Vec_char
 * @param f   Pointer to FILE
 * @param max Maximum number of bytes to read
 * @return    0 on success, -1 on failure
 */
int vec_read_file_upto(Vec_char *v, FILE *f, size_t max);

#endif // VEC_H_

#ifdef VEC_IMPLEMENTATION

#include <stdio.h>

#define VEC_BUF_S 1024

/**
 * Read the entire file f into vector v.
 *
 * @param v Pointer to Vec_char
 * @param f Pointer to FILE
 * @return  0 on success, -1 on failure
 */
int vec_read_entire_file(Vec_char *v, FILE *f)
{
	int ret = 0;
	size_t nread = 0;
	char buf[VEC_BUF_S];

	if (f == NULL) {
		fprintf(stderr, "vec_read_file_upto: file is NULL\n");
		ret = -1;
		goto finish;
	}

	while ((nread = fread(buf, sizeof(*buf), VEC_BUF_S, f)) != 0)
		vec_push_arr(v, buf, nread);

finish:
	return ret;
}

/**
 * Read the file f into vector v up to size max.
 *
 * @param v   Pointer to Vec_char
 * @param f   Pointer to FILE
 * @param max Maximum number of bytes to read
 * @return    0 on success, -1 on failure
 */
int vec_read_file_upto(Vec_char *v, FILE *f, size_t max)
{
	int ret = 0;
	size_t nread = 0;
	char buf[VEC_BUF_S];
	/* read_max = min(max, VEC_BUF_S) */
	const size_t read_max = (max < VEC_BUF_S) ? max : VEC_BUF_S;

	if (f == NULL) {
		fprintf(stderr, "vec_read_file_upto: file is NULL\n");
		ret = -1;
		goto finish;
	}

	if (v->c > max) {
		fprintf(stderr, "vec_read_file_upto: vector is already over maximum size\n");
		ret = -1;
		goto finish;
	}

	while (v->c + read_max <= max &&
	       (nread = fread(buf, sizeof(*buf), read_max, f)) != 0)
		vec_push_arr(v, buf, nread);

finish:
	return ret;
}

#endif // VEC_IMPLEMENTATION
