/*
  This is in the public domain, or distributed using MIT-0, as below

  Copyright (c) 2024 Spencer Pavkovic

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef SAB_H_
#define SAB_H_

#include <stdbool.h>

typedef unsigned char      uChar;
typedef unsigned short     uShort;
typedef unsigned int       uInt;
typedef unsigned long      uLong;
typedef unsigned long long ulLong;

#if __WORDSIZE == 64
typedef char            i8_t;
typedef unsigned char   u8_t;
typedef short           i16_t;
typedef unsigned short  u16_t;
typedef int             i32_t;
typedef unsigned int    u32_t;
typedef long            i64_t;
typedef unsigned long   u64_t;
typedef float           f32_t;
typedef double          f64_t;
#elif __WORDSIZE == 32
typedef char                i8_t;
typedef unsigned char       u8_t;
typedef short               i16_t;
typedef unsigned short      u16_t;
typedef int                 i32_t;
typedef unsigned int        u32_t;
typedef long long           i64_t;
typedef unsigned long long  u64_t;
typedef float               f32_t;
typedef double              f64_t;
#endif // __WORDSIZE

#endif // SAB_H_
