/*
  Copyright (c) 2023 Spencer Pavkovic

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef BARTENDER_H_
#define BARTENDER_H_

void bt_cocktail_sort(int a[], size_t size);
void bt_selection_sort(int a[], size_t size);
void bt_quick_sort(int a[], size_t size);
void bt_qsort(int a[], ssize_t lo, ssize_t hi);

#ifdef BARTENDER_IMPLEMENTATION

static void bt_swap(int a[], size_t x, size_t y)
{
	int z = a[x];
	a[x] = a[y];
	a[y] = z;
}

void bt_cocktail_sort (int a[], size_t size)
{
	unsigned short swapped;
	size_t x = 0;
	// run through the list
	do {
		swapped = 0;
		// swap from bottom to top, putting high values in place
		for (size_t i = x; i < size-1; ++i) {
			if (a[i] > a[i+1]) {
				// if a[i] in greater than a[i+1], swap the two
				bt_swap(a, i, i+1);
				swapped = 1;
			}
		}

		// if nothing was swapped, list is sorted, exit
		if (!swapped)
			break;
		swapped = 0;

		// swap from top to bottom, putting low values in place
		for(size_t i = --size; i > x; --i) {
			if (a[i-1] > a[i]) {
				// if a[i-1] in greater than a[i], swap the two
				bt_swap(a, i-1, i);
				swapped = 1;
			}
		}

		// if nothing was swapped, list is sorted, exit
		++x;
	} while (swapped);
}

void bt_selection_sort(int a[], size_t size)
{
	for (size_t i = 0; i < size; ++i) {
		int sm = a[i];
		int sm_i = i;

		for (size_t j = 0; j < size - i; ++j) {
			if (a[i+j] < sm) {
				sm = a[i+j];
				sm_i = i+j;
			}
		}

		bt_swap(a, i, sm_i);
	}
}

void bt_qsort(int a[], ssize_t lo, ssize_t hi)
{
	// Return if lo or hi are out of range
	if (hi <= lo || lo < 0)
		return;

	// set the pivot to the last value, and a temp pivot index
	int pv = a[hi];
	ssize_t pv_i = lo - 1;

	for (size_t i = lo; i < (size_t)hi; ++i) {
		// if a[i] is greater than pivot, move the pivot over
		// then swap a[i] and the pivot, moving high values past past the pivot
		if (a[i] < pv) {
			++pv_i;
			bt_swap(a, pv_i, i);
		}
	}

	// move the pivot into position
	++pv_i;
	bt_swap(a, pv_i, hi);

	// sort the left and right half using the same algorithm
	bt_qsort(a, lo, (pv_i-1));
	bt_qsort(a, (pv_i+1), hi);
}

void bt_quick_sort(int a[], size_t size)
{
	bt_qsort(a, 0, (size-1));
}

#endif // BARTENDER_IMPLEMENTATION

#endif // BARTENDER_H_
