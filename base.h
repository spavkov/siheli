/**
 * \file base.h
 * \brief A simple base-16/32/64 encoding and decoding library.
 *
 * Copyright 2024
 *
 * \author Spencer Pavkovic
 */
/* SPDX-License-Identifier: MIT */
#ifndef BASE_H_
#define BASE_H_

#define PADCHAR '='

/* Base-16 */
/**
 * \brief Encode a string with base-16
 *
 * Encode given data "in" with base-16 and store it into the string "out." The output string must be
 * at least twice as long as the input data, as every byte input is encoded into two output bytes.
 *
 * \param in  Input data
 * \param len Length of the input data
 * \param out Output base-16 string
 */
long b16_enc(void *in, long len, char *out);

/**
 * \brief Decode a base-16 string
 *
 * Decode given base-16 string "in" and store it into "out." The output pointer must be at least
 * half as long as the input string, as every two bytes from input is encoded into one output byte.
 *
 * \param in  Input base-16 string
 * \param len Length of the input string
 * \param out Output data
 */
long b16_dec(char *in, long len, void *out);


/* Base-32 */
/**
 * \brief Encode a string with base-32
 *
 * Encode given data "in" with base-32 and store it into the string "out." The output string must be
 * at least 1.6x as long as the input, as every 5 input bytes is encoded into 8 output bytes.
 *
 * \param in  Input data
 * \param len Length of the input data
 * \param out Output base-32 string
 */
long b32_enc(void *in, long len, char *out);

/**
 * \brief Decode a base-32 string
 *
 * Decode given base-32 string "in" and store it into "out." The output pointer must be at least
 * 5/8ths as long as the input string, as every 8 bytes from input is encoded into 5 output bytes.
 *
 * \param in  Input base-32 string
 * \param len Length of the input string
 * \param out Output data
 */
long b32_dec(char *in, long len, void *out);


/* Base-64 */
/**
 * \brief Encode a string with base-64
 *
 * Encode given data "in" with base-64 and store it into the string "out." The output string must be
 * at least 1.34x as long as the input data, as every 3 input bytes is encoded into 4 output bytes.
 *
 * \param in  Input data
 * \param len Length of the input data
 * \param out Output base-64 string
 */
long b64_enc(void *in, long len, char *out);

/**
 * \brief Decode a base-64 string
 *
 * Decode given base-32 string "in" and store it into "out." The output pointer must be at least
 * 3/4ths as long as the input string, as every 4 bytes from input is encoded into 3 output bytes.
 *
 * \param in  Input base-64 string
 * \param len Length of the input string
 * \param out Output data
 */
long b64_dec(char *in, long len, void *out);

#ifdef BASE_PRINT

/**
 * \brief Print data as a base-16 string
 *
 * Prints data as a base-16 string with a line wrap at 80 characters.
 *
 * \param in  Input data
 * \param len Length of the input data
 */
void b16_print(void *in, long len);

/**
 * \brief Print data as a base-32 string
 *
 * Prints data as a base-32 string with a line wrap at 80 characters.
 *
 * \param in  Input data
 * \param len Length of the input data
 */
void b32_print(void *in, long len);

/**
 * \brief Print data as a base-64 string
 *
 * Prints data as a base-64 string with a line wrap at 80 characters.
 *
 * \param in  Input data
 * \param len Length of the input data
 */
void b64_print(void *in, long len);

#endif /* BASE_PRINT */

#endif /* BASE_H_ */

#ifdef BASE_IMPLEMENTATION

static int s_bIndex_cs(const char *table, const char c)
{
	int ret = 0;

	while (table[ret] != c && table[ret] != 0)
		++ret;

	if (table[ret] != c)
		ret = -1;
	return ret;
}

static char s_bUpper(const char c)
{
	char ret = c;
	if ('a' <= ret && ret <= 'z')
		ret -= 0x20;
	return ret;
}

static int s_bIndex_ci(const char *table, const char c)
{
	int ret = 0;

	while (s_bUpper(table[ret]) != s_bUpper(c) && table[ret] != 0)
		++ret;

	if (s_bUpper(table[ret]) != s_bUpper(c))
		ret = -1;
	return ret;
}

static int s_bIndex(const char *table, const char c, int case_sensitive)
{
	int ret;

	if (case_sensitive)
		ret = s_bIndex_cs(table, c);
	else
		ret = s_bIndex_ci(table, c);

	return ret;
}

static void s_bPad(char *buf, int padding, int s)
{
	while (padding)
		buf[s - padding--] = PADCHAR;
}

static unsigned long s_bPack(void *buf, int bytes)
{
	unsigned long ret = 0;

	--bytes;

	for (int i = 0; i < bytes; ++i) {
		ret += ((unsigned char*)buf)[i];
		ret <<= 8;
	}
	ret += ((unsigned char*)buf)[bytes];

	return ret;
}

/**
 * \param table      Lookup table for characters
 * \param buf        Pointer to read from
 * \param bytes      Number of bytes to read, minimum block size. 8 for b32 and 4 for b64
 * \param bits       Number of bits to shift every loop
 * \param case_sense Look through the table in a case-sensitive way
 */
static unsigned long s_bUnpack(const char *table, void *buf, int bytes, int bits, int case_sens)
{
	unsigned long ret = 0;
	int i = 0;
	int x;

	for (; i < bytes; ++i) {
		if (((char*)buf)[i] != PADCHAR) {
			x = s_bIndex(table, ((char*)buf)[i], case_sens);
			if (x == -1)
				return (unsigned long)-1;
			ret += x;
		}

		if (i != bytes - 1)
			ret <<= bits;
	}

	return ret;
}

/* Base-16 */
long b16_enc(void *in, long len, char *out)
{
	const char table[] = "0123456789ABCDEF";
	long ret = 0;

	for (long i = 0; i < len; ++i) {
		int j = i * 2;

		out[  j] = table[((char*)in)[i] >> 4];
		out[++j] = table[((char*)in)[i] & 15];

		ret += 2;
	}

	return ret;
}

long b16_dec(char *in, long len, void *out)
{
	const char table[] = "0123456789ABCDEF";
	long ret = 0;

	for (long i = 0; i < len; ++i) {
		int j = i / 2;

		int x = s_bIndex(table, in[i], 0);
		if (x == -1)
			return -1;

		((unsigned char*)out)[j] = x << 4;

		x = s_bIndex(table, in[i], 0);
		if (x == -1)
			return -1;

		((unsigned char*)out)[j] += x;

		++ret;
	}

	return ret;
}


/* Base-32 */
long b32_enc(void *in, long len, char *out)
{
	const char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
	long ret = 0;

	int padding = (5 - (len % 5)) % 5;

	for (long i = 0; i < len; i += 5) {
		char buf[8] = {0};
		unsigned long stream = s_bPack((char*)in + i, 5);

		for (int j = 0; j < 8; ++j) {
			int bits = (stream >> (5 * (7 - j))) & 0x1f;
			buf[j]   = table[bits];
		}

		if (i + 5 > len)
			s_bPad(buf, padding, 8);

		for (int j = 0; j < 8; ++j)
			out[ret + j] = buf[j];
		ret += 8;
	}

	return ret;
}

long b32_dec(char *in, long len, void *out)
{
	const char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
	long ret = 0;

	for (long i = 0; i < len; i += 8) {
		/* table, in + offset, chars ,[per] ascii, case sensitive */
		unsigned long stream = s_bUnpack(table, (char*)in + i, 8, 5, 0);
		int padding = 0;

		if (stream == (unsigned char)-1)
			return -1;

		for (int j = 0; j < 8; ++j) {
			if (in[i + j] == PADCHAR)
				++padding;
		}

		for (int j = 0; j < 5 - padding; ++j) {
			((unsigned char*)out)[ret + j] = (stream >> (8 * (4 - j))) & 0xff;
		}
		ret += 5 - padding;
	}

	return ret;
}


/* Base-64 */
long b64_enc(void *in, long len, char *out)
{
	const char table[64] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
	long ret = 0;

	int  padding   = (3 - (len % 3)) % 3;

	for (int i = 0; i < len; i += 3) {
		char buf[4] = {0};
		int stream = s_bPack((char*)in + i, 3);

		for (int k = 0; k < 4; ++k) {
			int bits = (stream >> (6 * (3 - k))) & 0x3f;
			buf[k]   = table[bits];
		}

		if (i + 3 > len)
			s_bPad(buf, padding, 4);

		for (int j = 0; j < 4; ++j)
			out[ret + j] = buf[j];
		ret += 4;
	}

	return ret;
}

long b64_dec(char *in, long len, void *out)
{
	const char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
	long ret = 0;

	for (long i = 0; i < len; i += 4) {
		unsigned long stream = s_bUnpack(table, (char*)in + i, 4, 6, 1);
		int padding = 0;

		if (stream == (unsigned char)-1)
			return -1;

		for (int j = 0; j < 4; ++j) {
			if (in[i + j] == PADCHAR)
				++padding;
		}

		for (int j = 0; j < 3 - padding; ++j) {
			((unsigned char*)out)[ret + j] = (stream >> (8 * (2 - j))) & 0xff;
		}
		ret += 3 - padding;
	}

	return ret;
}

#ifdef BASE_PRINT

#include <stdio.h>

static inline long s_bMin(long a, long b)
{
	return (a < b) ? a : b;
}

#define s_bPRINT_INTERNAL(ratio, func)                                     \
	char buf[80];                                                      \
	for (long i = 0; i < len; i += ratio) {                            \
		long l = func((char*)in + i, s_bMin(ratio, len - i), buf); \
		for (int j = 0; j < l; ++j)                                \
			printf("%c", buf[j]);                              \
		printf("\n");                                              \
	}

void b16_print(void *in, long len)
{
	s_bPRINT_INTERNAL(40, b16_enc)
}

void b32_print(void *in, long len)
{
	s_bPRINT_INTERNAL(50, b32_enc)
}

void b64_print(void *in, long len)
{
	s_bPRINT_INTERNAL(60, b64_enc)
}

#endif /* BASE_PRINT */

#endif /* BASE_IMPLEMENTATION */
